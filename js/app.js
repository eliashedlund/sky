const app = {
  init: () => {
    document
      .getElementById('btnSearch')
      .addEventListener('click', app.fetchCityLocation);
  },
  fetchCityLocation: (ev) => {
    ev.preventDefault();

    let key = 'ebc873d231afbba10c3b3d4168cebe2d';
    let limit = 1;
    let cityName = document.getElementById('search').value;
    let url = `http://api.openweathermap.org/geo/1.0/direct?q=${cityName}&limit=${limit}&appid=${key}`;

    //fetch the longitude and latitude of a city
    fetch(url)
      .then((resp) => {
        if (!resp.ok) throw new Error(resp.statusText);
        return resp.json();
      })
      .then((data) => {
        app.fetchWeather(data);
      })
      .catch(console.err);
  },
  fetchWeather: (data) => {
    let lat = data[0].lat;
    let lon = data[0].lon;
    let key = 'ebc873d231afbba10c3b3d4168cebe2d';
    let lang = 'en';
    let units = 'metric';
    let url = `http://api.openweathermap.org/data/2.5/forecast?lat=${lat}&lon=${lon}&appid=${key}&units=${units}&lang=${lang}`;

    //fetch the weather
    fetch(url)
      .then((resp) => {
        if (!resp.ok) throw new Error(resp.statusText);
        return resp.json();
      })
      .then((data) => {
        app.showWeather(data);
      })
      .catch(console.err);
  },
  showWeather: (resp) => {
    let container = document.querySelector('#weather');
    let html = ``;

    const today = new Date();
    const day = 60 * 60 * 24 * 1000;
    const dateBins = {};
    const nBins = 6;

    //Set up a bin (empty array) for each date
    for (let i = 0; i < nBins; i++) {
      const date = new Date(today.getTime() + i * day);
      dateBins[date.getDate()] = [];
    }

    for (const report of resp.list) {
      const reportDate = new Date(report.dt * 1000).getDate();
      dateBins[reportDate].push(report);
    }

    const keys = Object.keys(dateBins);

    keys.forEach((key, index) => {

      //Check if day has data
      if(dateBins[key].length) {
        let date = new Date(dateBins[key][0].dt * 1000);
        let weekday = new Intl.DateTimeFormat('en-US', { weekday: 'long'}).format(date);

        //Add html and weather data for each day
        html += `
          <div class="card">
            <h3>${weekday}</h3>
            <img
              src="http://openweathermap.org/img/wn/${dateBins[key][0].weather[0].icon}@4x.png"
              class="card__icon"
              alt="Weather description"
            />
            <table>
              <thead>
                <tr>
                  <td></td>
                  <td>Temp</td>
                  <td>Wind</td>
                  <td></dt>
                </tr>
              </thead>
              <tbody>
        `;

        //Add html and weather data for each time slot
        html += dateBins[key].map((day, index) => {
          let localTime = new Date(day.dt * 1000).toLocaleTimeString('sv-SE', { hour: '2-digit', minute: '2-digit' });

          return `
            <tr class="list">
              <td>${localTime}</td>
              <td>${day.main.temp}°</td>
              <td>${day.wind.speed}m/s</td>
              <td>
                <img
                  src="http://openweathermap.org/img/wn/${day.weather[0].icon}.png"
                  class="list__icon"
                  alt="Weather description"
                />
              </td>
            </tr>
          `;
        }).join(' ');

        //Add closing html tags
        html += `
            </thead>
          </table>
        </div>
        `;
      }
    });

    container.innerHTML = html;
  },
};

app.init();
